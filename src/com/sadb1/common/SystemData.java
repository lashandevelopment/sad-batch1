/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sadb1.common;

/**
 *
 * @author lashanc
 */
public class SystemData {
    private static String currentUser;
    private static String userType;
    private static String employee;

    public static String getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(String aCurrentUser) {
        currentUser = aCurrentUser;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String aUserType) {
        userType = aUserType;
    }

    public static String getEmployee() {
        return employee;
    }

    public static void setEmployee(String aEmployee) {
        employee = aEmployee;
    }
    
    
}
