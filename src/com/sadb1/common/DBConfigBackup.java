/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sadb1.common;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author lashan_h
 */
public class DBConfigBackup {
    public boolean backupDataWithDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
        boolean status = false;
        try {
            dumpExePath = "C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysqldump.exe";
            Process p = null;
            host = "127.0.0.1";
            port = "3306";
            user = "root";
            password = "123";
            database = "sadb1";
            
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
//            String filepath = "backup(with_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
            String filepath = backupPath + ".sql";

//            String batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            String batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + filepath + "\"";

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();

            if (processComplete == 0) {
                status = true;
                System.out.println("Success");
            } else {
                status = false;
                System.out.println("Error");
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return status;
    }
    public static void main(String[] args) {
        DBConfigBackup bConfigBackup = new DBConfigBackup();
        bConfigBackup.backupDataWithDatabase("", "", "", "", "", "", "/Users/lashanc/Desktop/backup"+new Date().getTime());
    }
    
}
